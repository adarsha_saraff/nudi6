import { Component, OnInit, OnDestroy } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import * as DecoupledEditor from 'nudi-ckeditor';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { MenuService } from '../../providers/menu.service';
import * as Rx from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { RtfpluginService } from '../../providers/rtfplugin.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {

  public Editor = DecoupledEditor;
  subscription: Rx.Subscription;
  public editor;
  pageZoom: string = "100%";

  public nudiDocument = {
    editorData: "",//"<p>abc&nbsp;<strong><u>efg</u></strong><i>hij</i><br><i>llll</i></p><p><span style=\"font-family:Consolas;\">lllljjhg f</span><br><span style=\"font-family:Consolas;\">kjjj</span></p>",
    version: 1,
    filePath: null,
    fileName: "untitled1.rtf",
    editingConfig: {
      fontSize: "11",
      fontFamily: "Airal"
    }
  }


  constructor(private translator: TranslateService,
    private electron: ElectronService,
    private rtfPlugin: RtfpluginService,
    private menuService: MenuService) {
    this.subscription = menuService.getMessage().subscribe(command => { this.processCommand(command); });
  }

  ngOnInit() {
    this.electron.browserWindow.setTitle(APP_TITLE + " - " + this.nudiDocument.fileName);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public onReady(editor) {
    this.editor = editor;
    const parent = editor.ui.view.editable.element.parentElement;
    parent.removeChild(editor.ui.view.editable.element);
    const divhost = document.querySelector("#page");
    divhost.appendChild(editor.ui.view.editable.element);
    editor.commands.get('enter').on('afterExecute', () => {
      if (this.nudiDocument.editingConfig.fontSize !== "11")
        this.editor.execute("fontSize", { value: this.nudiDocument.editingConfig.fontSize });
      if (this.nudiDocument.editingConfig.fontFamily !== "Airal")
        this.editor.execute("fontFamily", { value: this.nudiDocument.editingConfig.fontFamily });
    });
    editor.model.document.on("change", (event) => {
      if (event.name === 'change') {
        const selection = this.editor.model.document.selection;
        if (selection.hasAttribute("fontSize")) {
          this.nudiDocument.editingConfig.fontSize = selection.getAttribute("fontSize");
        }
        if (selection.hasAttribute("fontFamily")) {
          this.nudiDocument.editingConfig.fontFamily = selection.getAttribute("fontFamily");
        }
      } else {
        if (this.nudiDocument.version != this.editor.model.document.version) {
          this.electron.browserWindow.setDocumentEdited(true);
        }
      }
    });
  }

  public onChange({ editor }: ChangeEvent) {

  }

  public processCommand(command) {
    const cmd = command.command;
    const option = command.value;
    if (cmd !== "font-size")
      this.editor.editing.view.focus();
    switch (cmd) {
      case 'cut':

        break;
      case 'copy':
        // const json = this.editor.model.getSelectedContent(this.editor.model.document.selection).toJSON();
        // var htmlData: string = "";
        // for (var k = 0; k < json.length; k++) {

        // }

        break;
      case 'paste':
        //paste functionality
        const doc = this.editor.model.change(writer => {
          const p = writer.createElement('paragraph');
          const docFrag = writer.createDocumentFragment();
          writer.append(p, docFrag);
          writer.insertText(this.electron.clipboard.readText(), p);
          return docFrag;
        });
        this.editor.model.insertContent(doc);
        break;
      case 'font-family':
        //   console.log(normalizeFontNameForCSS(option));
        this.nudiDocument.editingConfig.fontFamily = option;
        this.editor.execute('fontFamily', { value: option });
        break;
      case 'font-size':
        if (parseInt(option) > 7) {
          this.nudiDocument.editingConfig.fontSize = option;
          this.editor.execute('fontSize', { value: option });
        }
        break;
      case 'font-weight':
      case 'editing-cmd':
        this.editor.execute(option);
        break;
      case 'text-color':
        this.editor.execute('textColor', { value: option });
        break;
      case 'text-align':
        this.editor.execute('alignment', { value: option });
        break;
      case 'font-size-up':
      case 'font-size-down':
        const selection = this.editor.model.document.selection;
        if (selection.hasAttribute("fontSize")) {
          var size = parseInt(selection.getAttribute("fontSize"));
          size = size + (cmd === 'font-size-down' ? -2 : 2);
          if (size > 7) {
            this.nudiDocument.editingConfig.fontSize = size.toString();
            this.editor.execute('fontSize', { value: size.toString() });
          }
        }
        break;
      case 'page-zoom':
        this.pageZoom = option + "%";
        break;
      case 'save-document':
        this.saveDocument();
        break;
      case 'open-document':
        this.openDocument();
        break;
      case 'new-document':
        this.createDocument();
        break;
    }
  }

  saveDocument() {
    if (!this.nudiDocument.filePath) {
      this.nudiDocument.filePath = this.electron.dialog.showSaveDialog(this.electron.browserWindow, {
        title: this.translator.instant('MENU.MENU_SAVE'),
        filters: [
          { name: 'Html', extensions: ['html'] }
        ]
      });
    }
    //this.rtfPlugin.convertToRtf(this.editor.getData());
    if (this.nudiDocument.filePath) {
      this.electron.fs.writeFile(this.nudiDocument.filePath, this.editor.getData(), (error) => {
        if (!error) {
          this.electron.browserWindow.setDocumentEdited(false);
          this.nudiDocument.version = this.editor.model.document.version;
        }
      });
      this.nudiDocument.fileName = this.electron.path.basename(this.nudiDocument.filePath);
      this.electron.browserWindow.setTitle(APP_TITLE + " - " + this.nudiDocument.fileName);
    }
  }

  openDocument() {
    this.nudiDocument.filePath = this.electron.dialog.showOpenDialog(this.electron.browserWindow, {
      title: this.translator.instant('MENU.MENU_SAVE'),
      filters: [
        { name: 'Html', extensions: ['html'] }
      ],
      properties: ['openFile']
    });
    console.log(this.nudiDocument.filePath);
    if (this.nudiDocument.filePath) {
      this.nudiDocument.filePath = this.nudiDocument.filePath[0];
      this.electron.fs.readFile(this.nudiDocument.filePath, { encoding: "utf-8" }, (error, data) => {
        if (error) {

        } else {
          this.nudiDocument.editorData = data;
        }
      })
      this.nudiDocument.fileName = this.electron.path.basename(this.nudiDocument.filePath);
      this.electron.browserWindow.setTitle(APP_TITLE + " - " + this.nudiDocument.fileName);
    }
  }

  createDocument() {
    if (this.nudiDocument.version == this.editor.model.document.version) {
      this.nudiDocument.editorData = "";
      this.nudiDocument.fileName = "untitled1";
      this.nudiDocument.filePath = null;
      this.nudiDocument.version = 1;
      this.electron.browserWindow.setTitle(APP_TITLE + " - " + this.nudiDocument.fileName);
    }
  }
}

const APP_TITLE = "ನುಡಿ";