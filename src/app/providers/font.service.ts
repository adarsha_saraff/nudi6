import { Injectable } from '@angular/core';
import { ElectronService } from './electron.service';
import * as Rx from 'rxjs/Rx';


@Injectable()
export class FontService {

  public Fonts;
  public FontProvider = new Rx.Subject();

  constructor(private elctron: ElectronService) {
    elctron.ipcRenderer.on('all-fonts-list', (event, args) => {
      console.log(args);
      this.Fonts = args;
      this.FontProvider.next(this.Fonts);
    });
  }

  public getFonts(){
    this.elctron.ipcRenderer.send('get-all-fonts', 1);
  }
}

export interface Font {
  path: string;
  postscriptName: string;
  family: string;
  style: string;
  weight: number;
  width: number;
  italic: boolean;
  monospace: boolean;
}