export default class Utils {

    public static getAlignment(cmd: string) {
        switch (cmd) {
            case 'qc':
                return 'center';
            case 'qj':
                return 'justify';
            case 'qr':
                return 'right';
            default:
                return 'left';
        }
    }

    public static toAlignment(alignment: string) {
        switch (alignment) {
            case 'justify':
                return '\\qj';
            case 'right':
                return '\\qr';
            case 'center':
                return '\\qc';
            default:
                return '\\ql';
        }
    }

    public static toPoint(size: number) {
        return size / 2;
    }

    public static fromPoint(size: number) {
        return size * 2;
    }



    /**
     * 
     * convert utf-8 charaters to unicode decimals
     * 
     * @param char 
     */
    public static encodeUtf(char: string) {
        var hex: Array<number> = [];
        for (var i = 0; i < char.length; i++) {
            var code = char.charCodeAt(i);
            if (code > 32767) {
                code = code - 65536;
            }
            hex.push(code);
        }
        return hex;
    }

    /**
     * convert unicode decimals to hex string
     * @param code 
     */
    public static decodeUtf(code: number) {
        if (code < 0) {
            code += 65536;
        }
        var codeHex = code.toString(16).toUpperCase();
        while (codeHex.length < 4) {
            codeHex = "0" + codeHex;
        }
        return codeHex
    }

    public static isAscii(char: string) {
        return char.charCodeAt(0) < 256;
    }

    public static isControlChar(char: string) {
        if (Utils.isAscii(char)) {
            switch (char) {
                case "{":
                case "}":
                case "\\":
                    return true;
            }
        }
        return false;
    }
}