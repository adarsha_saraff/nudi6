
export default class RtfNode {

    public parent: RtfNode;
    public children: Array<RtfNode> = [];
    public cssStyle: CSSStyles;
    public name: string;
    public text : string;

    constructor() {
        this.cssStyle = {
            fontFamily: undefined,
            fontSize: 11,
            strikethrough: false,
            bold: false,
            underline: false,
            italic: false,
            textAlignment: "left",
            backgroundColor: undefined,
            color: undefined,
        }
    }

    public setStyle(style : CSSStyles){
        this.cssStyle = style;
    }

    toRTF() {
        return this.name;
    }

    toJSON() {
        return JSON.stringify({
            name: this.name
        });
    }
}

export interface CSSStyles {
    fontSize: number,
    fontFamily: string,
    color: string,
    backgroundColor: string,
    bold: boolean,
    italic: boolean,
    underline: boolean,
    strikethrough: boolean,
    textAlignment: string
}

