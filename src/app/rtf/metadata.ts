/**
 * @author Adarsha Saraff
 * 
 * rtf document meta-data
 *
 * rtf 1.9 spec for meta-data 
 * {\info
 * {\title Template}
 * {\author John Doe}
 * {\creatim\yr1999\mo4\dy27\min1}
 * {\version2}
 * }
 */

export default class MetaData {

    /**
     * rtf cmd \author
     */
    public author: string;
    /**
     * rtf cmd \creatim
     */
    public creationDate: Date;
    /**
     * rtf cmd \title
     */
    public title: string;
    /**
     * rtf cmd \subject
     */
    public subject: string;
    /**
     * rtf cmd \doccomm
     */
    public description: string;
    /**
     * rtf cmd \versionN
     */
    public version: number;

    //TODO: add keyword support

    public constructor() {
        this.creationDate = new Date();
        this.version = 1;
    }

    /**
     * @returns json string
     */
    public toJSON() {
        return JSON.stringify({
            author: this.author,
            creationDate: this.creationDate,
            title: this.title,
            subject: this.subject,
            description: this.description,
            version: this.version
        });
    }

    public fromJSON(meta: string) {
        const metaJSON = JSON.parse(meta);
        this.author = metaJSON.author;
        this.creationDate = new Date(metaJSON.creationDate);
        this.title = metaJSON.title;
        this.subject = metaJSON.subject;
        this.description = metaJSON.description;
        this.version = metaJSON.version;
    }

    /**
     * @returns rtf string
     */
    public toRTF() {
        return `{\\*\\info{\\title ${this.title}}{\\author ${this.author}}{\\subject ${this.subject}}{\\creatim\\yr${this.creationDate.getFullYear()}\\mo${this.creationDate.getMonth()}\\dy${this.creationDate.getDate()}}{\\version${this.version}}{\\doccomm ${this.description}}}`;
    }
}