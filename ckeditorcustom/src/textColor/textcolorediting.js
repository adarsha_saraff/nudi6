import { upcastElementToAttribute } from '@ckeditor/ckeditor5-engine/src/conversion/upcast-converters';
import TextColorCommand from './textcolorcommand';
import { downcastAttributeToElement } from '@ckeditor/ckeditor5-engine/src/conversion/downcast-converters';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

const TEXT_COLOR = 'textColor';

export default class TextColorEditing extends Plugin {

    constructor(editor) {
        super(editor);

        editor.conversion.for("upcast").add(upcastElementToAttribute({
            view: {
                name: 'span',
                attributes: {
                    title: TEXT_COLOR
                }
            },
            model: {
                key: TEXT_COLOR,
                value: viewElement => {
                    const color = viewElement.getStyle('color');
                    if (!color) return "#000000";
                    if (color.charAt(0) === '#') {
                        return color;
                    }
                    const regEx = /^rgb\((\d*),\s?(\d*),\s?(\d*)\)$/;
                    const rgb = regEx.exec(color);
                    if (!rgb) {
                        return "#000000";
                    }
                    return toHex(rgb[1], rgb[2], rgb[3]);
                }
            }
        }));

        editor.conversion.for("downcast").add(downcastAttributeToElement({
            model: TEXT_COLOR,
            view: (modelAttributeValue, viewWriter) => {
                return viewWriter
                    .createAttributeElement('span',
                        {
                            style: 'color:' + modelAttributeValue
                        });
            }
        }));
        // Add TextColor command.
        editor.commands.add(TEXT_COLOR, new TextColorCommand(editor, TEXT_COLOR));
    }

    init() {
        const editor = this.editor;
        editor.model.schema.extend('$text', { allowAttributes: TEXT_COLOR });
        // const options = editor.config.get('textColor.options');
        // const definition = _buildDefinition(options);
        // Set-up the two-way conversion.
        //  editor.conversion.attributeToElement(definition);
    }
}

function toHex(r, g, b) {
    return '#' + parseInt(r).toString(16).padStart(2, '0') + parseInt(g).toString(16).padStart(2, '0') + parseInt(b).toString(16).padStart(2, '0');
}

// function _buildDefinition(options) {
//     const definition = {
//         model: {
//             key: TEXT_COLOR,
//             values: []
//         },
//         view: {},
//         upcastAlso: {}
//     };

//     for (const option of options) {
//         definition.model.values.push(option.model);
//         definition.view[option.model] = {
//             name: 'span',
//             styles: {
//                 'color': option.color
//             },
//             priority: 5
//         };
//     }
//     return definition;
// }
