import RtfNode from "./node";
import RtfParagraphNode from "./paragraph";

export default class RtfListNode extends RtfNode {

    constructor() {
        super();
        this.name = "ul";
    }

    public addListItem(rtfPg: RtfParagraphNode) {
        rtfPg.parent = this;
        this.children.push(rtfPg);
    }

    setAsOrdered(){
        this.name = "ol";
    }

    setAsUnordered(){
        this.name = "ul";
    }

}