/**
 * @author Adarsha Saraff
 * 
 * rtf 1.9 font table spec implementation
 * 
 */
export default class FontTable {

    fonts: Array<FontTbl> = [];


    public addFont(font: string) {
        var ft: FontTbl = this.findFont(font);
        if (!ft) {
            ft = {
                name: font,
                charset: 1,
                index: this.fonts.length
            }
            this.fonts.push(ft);
        }
        return ft;
    }

    public getFont(id: number) {
        return (this.fonts).find(f => (
            f.index == id
        ), id);
    }

    public findFont(name: string) {
        if (this.fonts === undefined)
            return null;
        return (this.fonts).find(f => (
            f.name == name
        ), name);
    }

    public addFontTbl(font: FontTbl) {
        if (this.fonts.length === 0) {
            this.fonts.push(font);
        }
    }

    public removeFontTbl(font: FontTbl) {
        if (this.fonts.length > 0) {
            let index = this.fonts.indexOf(font);
            if (index > -1) {
                this.fonts.slice(index, 1);
            }
        }
    }

    public toRTF() {
        return "{\\fonttbl" + this.fonts.map(x => (
            `{\\f${x.index}\\fnil\\fcharset${x.charset} ${x.name};}`
        )).join('') + "}";
    }

}

export interface FontTbl {
    index: number,
    name: string,
    charset: number
}