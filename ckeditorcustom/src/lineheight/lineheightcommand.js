import Command from '@ckeditor/ckeditor5-core/src/command';
import first from '@ckeditor/ckeditor5-utils/src/first';

export default class LineHeightCommand extends Command {

    refresh() {
        const firstBlock = first(this.editor.model.document.selection.getSelectedBlocks());
        // As first check whether to enable or disable the command as the value will always be false if the command cannot be enabled.
        this.isEnabled = !!firstBlock && this.editor.model.schema.checkAttribute( firstBlock, "lineheight" );        ;
        this.value = (this.isEnabled && firstBlock.hasAttribute('lineheight')) ? firstBlock.getAttribute('lineheight') : '1.15';
    }

    execute(options = {}) {
        const editor = this.editor;
        const model = editor.model;
        const doc = model.document;

        const value = options.value;

        model.change(writer => {

            const blocks = Array.from(doc.selection.getSelectedBlocks());
            const removeHeight = isDefault(value) || !value;

            if (removeHeight) {
                removeHeightFromSelection(blocks, writer);
            } else {
                setHeightOnSelection(blocks, writer, value);
            }
        });
    }

}

function isDefault(value) {
    return value === "1.15"
}

function removeHeightFromSelection(blocks, writer) {
    for (const block of blocks) {
        writer.removeAttribute("lineheight", block);
    }
}


function setHeightOnSelection(blocks, writer, height) {
    for (const block of blocks) {
        writer.setAttribute("height", height, block);
    }
}