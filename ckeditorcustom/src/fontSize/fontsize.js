import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontSizeEditing from './fontsizeediting';

export default class FontSize extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ FontSizeEditing ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'FontSize';
	}
}
