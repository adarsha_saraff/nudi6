import RtfNode from './node';

export default class RtfTextNode extends RtfNode {

   

    constructor(text){
        super();
        this.name = "span";
        this.text = text;
    }
}