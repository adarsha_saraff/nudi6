import { Component, OnInit } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';

@Component({
  selector: 'app-titlebar',
  templateUrl: './titlebar.component.html',
  styleUrls: ['./titlebar.component.scss']
})

export class TitlebarComponent implements OnInit {

 
  pageZoom = 100;

  constructor(private electronService: ElectronService) { }

  ngOnInit() {
   
  }

  onZoomChanged(e){
    
  }
}
