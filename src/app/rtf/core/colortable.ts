export default class ColourTable {

    colors: Array<ColorTbl> = [];

    addColor(red: number, blue: number, green: number) {
        const color = red + blue + green;
        var ct: ColorTbl = this.findColor(color);
        if (ct === undefined || ct === null) {
            ct = {
                index: this.colors.length,
                red: red,
                blue: blue,
                green: green
            };
            this.colors.push(ct);
        }
        return ct;
    }

    getColor(index: number) {
        return (this.colors as any).find(c => (
            c.index === index
        ), index);
    }

    findColor(color: number) {
        return (this.colors as any).find(c => (
            (c.red + c.blue + c.green) === color
        ), color);
    }

    toRTF(){
        return "{\colortbl;" +
        this.colors.map(c=>(
            `\\red${c.red}\\green${c.green}\\blue${c.blue};`
        )).join('') + "}";
    }

}

export interface ColorTbl {
    index: number;
    red: number;
    green: number;
    blue: number;
}