import { Injectable } from '@angular/core';
import * as Rx from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})

export class MenuService {

  private subject = new Rx.Subject<any>();
  constructor() { }

  sendMessage(cmd: string, option: string) {
      this.subject.next({
        command : cmd,
        value : option
      });
  }

  clearMessage() {
      this.subject.next();
  }

  getMessage(): Rx.Observable<any> {
      return this.subject.asObservable();
  }
}
