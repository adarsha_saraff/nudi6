import RtfNode, { CSSStyles } from "./node";
import RtfTextNode from './text';
import RtfListNode from "./listnode";

export default class RtfParagraphNode extends RtfNode {

    public clearStyle : boolean = false;

    constructor() {
        super();
        this.name = "p";
    }

    public insertText(text : RtfTextNode){
        text.parent = this;
        this.children.push(text);
    }

    public insertList(list : RtfListNode){
        list.parent = this;
        this.children.push(list);
    }

}