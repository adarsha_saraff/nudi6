import RtfNode from "./node";

export default class RtfRootNode extends RtfNode{

    constructor(){
        super();
        this.name = "_root";
    }

}