import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import LineHeightCommand from './LineHeightCommand';

const LINEHEIGHT = "lineheight"
export default class LineHeightEditing extends Plugin {

    constructor(editor) {
        super(editor);

        editor.config.define(LINEHEIGHT, {
            options: [
                "1.0",
                "1.15",
                "1.5",
                "2.0"
            ]
        });
    }

    init() {
        const editor = this.editor;
        editor.model.schema.extend('$block', { allowAttributes: LINEHEIGHT });
        const enabledOptions = editor.config.get('alignment.options');
        const definition = _buildDefinition(enabledOptions);
        editor.conversion.attributeToAttribute(definition);
        editor.commands.add(LINEHEIGHT, new LineHeightCommand(editor));
    }

}

function _buildDefinition(options) {
    const definition = {
        model: {
            key: LINEHEIGHT,
            values: options.slice(-2)
        },
        view: {}
    };


    for (const option of options) {
        definition.view[option] = {
            key: 'style',
            value: {
                'text-align': `${option}pt`
            }
        };
    }
    return definition;

}