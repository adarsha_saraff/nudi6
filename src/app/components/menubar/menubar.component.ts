import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { FontService, Font } from '../../providers/font.service';
import * as Rx from 'rxjs/Rx';
import { MenuService } from '../../providers/menu.service';
import { SelectItem } from 'primeng/api';


@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss']
})

export class MenubarComponent implements OnInit, OnDestroy {

  subscription: Rx.Subscription;
  fontSize: SelectItem[] = [
    { label: "8", value: "8" },
    { label: "9", value: "9" },
    { label: "10", value: "10" },
    { label: "11", value: "11" },
    { label: "12", value: "12" },
    { label: "14", value: "14" },
    { label: "16", value: "16" },
    { label: "18", value: "18" },
    { label: "20", value: "20" },
    { label: "24", value: "24" },
    { label: "28", value: "28" },
    { label: "32", value: "32" },
    { label: "68", value: "68" },
    { label: "72", value: "72" }];
  selectedFontSize = "11";
  showLayout: boolean = false;
  fonts: Font[];
  selectedFont: Font;
  index: number = 1;
  textColor: string;



  constructor(private electron: ElectronService,
    private fontService: FontService,
    private menuService: MenuService) {

    this.subscription = this.fontService.FontProvider
      .asObservable().subscribe(fonts => {
        const f = fonts as Font[];
        var hashesFound = {};
        f.forEach(function (o) {
          hashesFound[o.family] = o;
        });
        this.fonts = Object.keys(hashesFound).map(function (k) {
          return hashesFound[k];
        });
      });
  }

  ngOnInit() {
    this.fontService.getFonts();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('window:keyup', ['$event'])
  processShortcuts(event: KeyboardEvent) {
    console.log(event);
    if (event.ctrlKey && event.shiftKey && event.code === "Equal") {
      this.menuService.sendMessage("font-weight", "subscript");
    } else if (event.ctrlKey && event.code === "Equal") {
      this.menuService.sendMessage("font-weight", "superscript");
    } else if (event.ctrlKey && event.code === "KeyS") {
      console.log("save")
      this.menuService.sendMessage("save-document", "");
    } else if (event.ctrlKey && event.code === "KeyP") {
      console.log("print")
      this.menuService.sendMessage("print-document", "");
    }
  }

  cmdPasteClick() {
    console.log("paste");
    this.menuService.sendMessage("paste", "");
  }

  cmdCutClick() {
    console.log("cut");
    this.menuService.sendMessage("cut", "");
  }

  cmdCopyClick() {
    console.log("copy");
    this.menuService.sendMessage("copy", "");
  }

  fontFamilyChanged(value) {
    console.log("font-family" + this.selectedFont.family);
    this.menuService.sendMessage("font-family", createFontFamilyForCss(this.selectedFont));
  }

  fontSizeChanged(value) {
    console.log("font-size", this.selectedFontSize);
    this.menuService.sendMessage("font-size", this.selectedFontSize);
  }



  textColorChanged() {
    console.log("text-Color", this.textColor);
    this.menuService.sendMessage("text-color", this.textColor);
  }

  cmdUndoClick() {
    this.menuService.sendMessage("editing-cmd", "undo");
  }

  cmdRedoClick() {
    this.menuService.sendMessage("editing-cmd", "redo");
  }

  cmdBoldClick() {
    this.menuService.sendMessage("font-weight", "bold");
  }

  cmdItalicClick() {
    this.menuService.sendMessage("font-weight", "italic");
  }

  cmdUnderlineClick() {
    this.menuService.sendMessage("font-weight", "underline");
  }

  cmdSuperscriptClick() {
    this.menuService.sendMessage("font-weight", "superscript");
  }
  cmdSubscriptClick() {
    this.menuService.sendMessage("font-weight", "subscript");
  }

  cmdStrikethroughClick() {
    this.menuService.sendMessage("font-weight", "strikethrough");
  }

  cmdAlignLeftClick() {
    this.menuService.sendMessage("text-align", "left");
  }

  cmdAlignRightClick() {
    this.menuService.sendMessage("text-align", "right");
  }

  cmdAlignCenterClick() {
    this.menuService.sendMessage("text-align", "center");
  }

  cmdAlignJustifyClick() {
    this.menuService.sendMessage("text-align", "justify");
  }

  cmdFontSizeDownClick() {
    this.menuService.sendMessage("font-size-down", "");
  }

  cmdFontSizeUpClick() {
    this.menuService.sendMessage("font-size-up", "");
  }

  cmdLayoutClick() {
    this.showLayout = true;
  }

  cmdMetaClick() {

  }

  cmdPrintClick() {
    this.menuService.sendMessage("print-document", "");
  }

  cmdSaveClick() {
    this.menuService.sendMessage("save-document", "");
  }

  cmdPageSetupClick() {

  }

  cmdOpenClick(){
    this.menuService.sendMessage("open-document", "");

  }

  cmdHelpClick(){
  }

  cmdNewClick(){
    this.menuService.sendMessage("new-document", "");
  }
}

function createFontFamilyForCss(font: Font) {
  return normalizeFontNameForCSS(font.family); // + ", " + (font.monospace ? "monospace" : "sans-serif");
}

function normalizeFontNameForCSS(fontName) {
  fontName = fontName.trim();
  // Compound font names should be quoted.
  if (fontName.indexOf(' ') > 0) {
    fontName = "'" + fontName + "'";
  }
  return fontName;
}