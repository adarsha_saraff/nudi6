const _0 = "0".charCodeAt(0);
const _9 = "9".charCodeAt(0);
const _z = "z".charCodeAt(0);
const _A = "A".charCodeAt(0);

export default class RtfReader {




    processKeyword(keyword: string) {

    }

}

function isAlpha(ch: string) {
    const code = ch.charCodeAt(0);
    return code >= _A && code <= _z;
}

function isDigit(ch: string) {
    const code = ch.charCodeAt(0);
    return code >= _0 && code <= _9;
}