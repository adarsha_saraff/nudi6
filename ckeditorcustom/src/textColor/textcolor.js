import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import TextColorEditing from './textcolorediting';

export default class FontSize extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ TextColorEditing ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'TextColor';
	}
}