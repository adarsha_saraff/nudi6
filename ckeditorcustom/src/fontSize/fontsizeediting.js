import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontSizeCommand from './fontsizecommand';
import { upcastElementToAttribute } from '@ckeditor/ckeditor5-engine/src/conversion/upcast-converters';
import { downcastAttributeToElement } from '@ckeditor/ckeditor5-engine/src/conversion/downcast-converters';

const FONT_SIZE = 'fontSize';
export default class FontSizeEditing extends Plugin {

    constructor(editor) {
        super(editor);
        // const options = normalizeOptions(this.editor.config.get('fontSize.options')).filter(item => item.model);
        // const definition = buildDefinition(FONT_SIZE, options);
        // Set-up the two-way conversion.
        // editor.conversion.attributeToElement(definition);

        editor.conversion.for("upcast").add(upcastElementToAttribute({
            view: {
                name: 'span',
                styles: {
                    "font-size" : /[\s\S]+/
                }
            },
            model: {
                key: FONT_SIZE,
                value: viewElement => {
                    const sizePt = viewElement.getStyle('font-size');
                    if (!sizePt) {
                        return "11";
                    }
                    const size = sizePt.replace(/"|'/g, '');
                    return size.slice(-2);
                }
            }
        }));

        editor.conversion.for("downcast").add(downcastAttributeToElement({
            model: FONT_SIZE,
            view: (modelAttributeValue, viewWriter) => {
                return viewWriter
                    .createAttributeElement('span',
                        {
                            style: 'font-size:' + `${modelAttributeValue}pt`
                        });
            }
        }));

        // Add FontSize command.
        editor.commands.add(FONT_SIZE, new FontSizeCommand(editor, FONT_SIZE));
    }

    init() {
        const editor = this.editor;
        // Allow fontSize attribute on text nodes.
        editor.model.schema.extend('$text', { allowAttributes: FONT_SIZE });
    }

}


// function normalizeOptions(configuredOptions) {

//     // Convert options to objects.
//     return configuredOptions
//         .map(getOptionDefinition)
//         // Filter out undefined values that `getOptionDefinition` might return.
//         .filter(option => !!option);
// }

// function getOptionDefinition(option) {
//     // Treat any object as full item definition provided by user in configuration.
//     if (typeof option === 'object') {
//         return option;
//     }
//     // 'Default' font size. It will be used to remove the fontSize attribute.
//     if (option === 'default') {
//         return {
//             model: undefined,
//             title: 'Default'
//         };
//     }
//     // At this stage we probably have numerical value to generate a preset so parse it's value.
//     const sizePreset = parseFloat(option);
//     // Discard any faulty values.
//     if (isNaN(sizePreset)) {
//         return;
//     }
//     // Return font size definition from size value.
//     return generatePointPreset(sizePreset);
// }

// function generatePointPreset(size) {
//     const sizeName = String(size);
//     return {
//         title: sizeName,
//         model: size,
//         view: {
//             name: 'span',
//             styles: {
//                 'font-size': `${size}pt`
//             },
//             priority: 5
//         }
//     };
// }

// function buildDefinition(modelAttributeKey, options) {
//     const definition = {
//         model: {
//             key: modelAttributeKey,
//             values: []
//         },
//         view: {},
//         upcastAlso: {}
//     };



//     for (const option of options) {
//         definition.model.values.push(option.model);
//         definition.view[option.model] = option.view;
//         if (option.upcastAlso) {
//             definition.upcastAlso[option.model] = option.upcastAlso;
//         }
//     }

//     return definition;
// }