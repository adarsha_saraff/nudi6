/**
 * @author Adarsha Saraff
 * 
 * Fontfamily overrides ckeditor5 built-in font plugin behaviour
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontFamilyEditing from "./fontfamilyediting";

export default class FontFamily extends Plugin{


    static get requires(){
        return [FontFamilyEditing];
    }

    static get pluginName(){
        return "FontFamily";
    }


}