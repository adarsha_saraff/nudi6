import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import SubscriptEditing from "./subscriptediting";

export default class Subscript extends Plugin{


    static get requires() {
		return [ SubscriptEditing ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Subscript';
	}

}