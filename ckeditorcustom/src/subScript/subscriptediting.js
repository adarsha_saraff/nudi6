import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import AttributeCommand from "@ckeditor/ckeditor5-basic-styles/src/attributecommand";


const SUBSCRIPT = "subscript";

export default class SubscriptEditing extends Plugin {

    init() {
        const editor = this.editor;

        editor.model.schema.extend('$text', { allowAttributes: SUBSCRIPT });
        editor.conversion.attributeToElement({
            model: SUBSCRIPT,
            view: 'sub'
        });

        editor.commands.add(SUBSCRIPT, new AttributeCommand(editor, SUBSCRIPT));
    }

}