import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import AttributeCommand from "@ckeditor/ckeditor5-basic-styles/src/attributecommand";


const SUPERSCRIPT = "superscript";

export default class SuperscriptEditing extends Plugin {

    init() {
        const editor = this.editor;

        editor.model.schema.extend('$text', { allowAttributes: SUPERSCRIPT });
        editor.conversion.attributeToElement({
            model: SUPERSCRIPT,
            view: 'sup'
        });

        editor.commands.add(SUPERSCRIPT, new AttributeCommand(editor, SUPERSCRIPT));
    }

}