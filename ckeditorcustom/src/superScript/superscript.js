import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import SuperscriptEditing from "./superscriptediting";

export default class Superscript extends Plugin{


    static get requires() {
		return [ SuperscriptEditing ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Superscript';
	}

}