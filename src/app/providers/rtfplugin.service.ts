import { Injectable } from '@angular/core';
import Rtf from "../rtf/rtf";
import RtfDocument from "../rtf/document";
import RtfNode, { CSSStyles } from '../rtf/core/node';
import RtfWriter from '../rtf/writer/writer';
import RtfParagraphNode from '../rtf/core/paragraph';
import RtfTextNode from '../rtf/core/text';

@Injectable({
  providedIn: 'root'
})
export class RtfpluginService {

  private rtfDoc: RtfDocument;
  private rtfWriter: RtfWriter;
  

  constructor() {
  }

  public convertToRtf(data: string) {
    this.rtfDoc = Rtf.createDocument();
    var root = document.createElement("div");
    root.innerHTML = data;
    this.convertChild(root);
  }

  private convertChild(parent: HTMLElement) {
    console.log(parent.childNodes);
    for (let i = 0; i < parent.childNodes.length; i++) {
      const child = parent.childNodes[i];
      if(child.nodeName === "P"){
         const rtfCurPara = this.rtfDoc.addParagraph("left");
        this.convertChildNode(child, rtfCurPara);
      }
    }
  }

  private convertChildNode(node : Node, rtfNode : RtfParagraphNode){
   // console.log(node.childNodes);
    if (!node.hasChildNodes()) {
      return;
    }
    if(node.nodeName === "SPAN"){ //this para has style set to it
      for(let i = 0; i < node.childNodes.length; i++){
        
      }
    }else if(node.nodeName === "#text"){ //just plan para with no style
      rtfNode.insertText(new RtfTextNode(node.textContent));
    }else if(node.nodeName === "STRONG"){ //bold text

    }else if(node.nodeName === "I"){ //italic text

    }else if(node.nodeName === "U"){ //underline text

    }else if(node.nodeName === "S"){ //strike through

    }else if(node.nodeName === "SUP"){ //superscript

    }else if(node.nodeName === "SUB"){//subscript
      
    }
  }

}
