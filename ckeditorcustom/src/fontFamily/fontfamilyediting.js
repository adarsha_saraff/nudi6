import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import FontFamilyCommand from './fontfamilycommand';
import { upcastElementToAttribute } from '@ckeditor/ckeditor5-engine/src/conversion/upcast-converters';
import { downcastAttributeToElement } from '@ckeditor/ckeditor5-engine/src/conversion/downcast-converters';


const FONT_FAMILY = "fontFamily";

export default class FontFamilyEditing extends Plugin {

	constructor(editor) {
		super(editor);
		// editor.config.define(FONT_FAMILY, {
		// 	options: [
		// 		'default',
		// 		'Arial',
		// 		'Courier New',
		// 		'Georgia',
		// 		'Lucida Sans Unicode',
		// 		'Tahoma',
		// 		'Comic Sans MS',
		// 		'Times New Roman',
		// 		'Trebuchet MS',
		// 		'Verdana'
		// 	]
		// });
	}

	init() {
		const editor = this.editor;
		// Allow fontFamily attribute on text nodes.
		editor.model.schema.extend('$text', { allowAttributes: FONT_FAMILY });
		// Get configured font family options without "default" option.
		//	const options = normalizeOptions(editor.config.get('fontFamily.options')).filter(item => item.model);
		//	const definition = buildDefinition(FONT_FAMILY, options);
		// Set-up the two-way conversion.
		//	editor.conversion.attributeToElement(definition);
		editor.conversion.for("upcast").add(upcastElementToAttribute({
			view: {
				name: 'span',
				styles: {
					"font-family" : "$text"
				}
			},
			model: {
				key: FONT_FAMILY,
				value: viewElement => {
					const fontName = viewElement.getStyle('font-family');
					if (!fontName) {
						return "Airal";
					}
					return fontName;
				}
			}
		}));
		editor.conversion.for("downcast").add(downcastAttributeToElement({
			model: FONT_FAMILY,
			view: (modelAttributeValue, viewWriter) => {
				return viewWriter
					.createAttributeElement('span',
						{
							style: 'font-family:' + modelAttributeValue
						});
			}
		}));
		editor.commands.add(FONT_FAMILY, new FontFamilyCommand(editor, FONT_FAMILY));
	}
}

// function normalizeOptions(configuredOptions) {
// 	// Convert options to objects.
// 	return configuredOptions
// 		.map(getOptionDefinition)
// 		// Filter out undefined values that `getOptionDefinition` might return.
// 		.filter(option => !!option);
// }

// function getOptionDefinition(option) {
// 	// Treat any object as full item definition provided by user in configuration.
// 	if (typeof option === 'object') {
// 		return option;
// 	}
// 	// Handle 'default' string as a special case. It will be used to remove the fontFamily attribute.
// 	if (option === 'default') {
// 		return {
// 			title: 'Default',
// 			model: undefined
// 		};
// 	}
// 	// Ignore values that we cannot parse to a definition.
// 	if (typeof option !== 'string') {
// 		return;
// 	}
// 	// Return font family definition from font string.
// 	return generateFontPreset(option);
// }

// function generateFontPreset(fontDefinition) {
// 	// Remove quotes from font names. They will be normalized later.
// 	const fontNames = fontDefinition.replace(/"|'/g, '');
// 	// The first matched font name will be used as dropdown list item title and as model value.
// 	const firstFontName = fontNames.replace(/ /g, '');
// 	// CSS-compatible font names.
// 	const cssFontNames = normalizeFontNameForCSS(fontNames);
// 	return {
// 		title: firstFontName,
// 		model: firstFontName,
// 		view: {
// 			name: 'span',
// 			styles: {
// 				'font-family': cssFontNames
// 			},
// 			priority: 5
// 		}
// 	};
// }



// function buildDefinition(modelAttributeKey, options) {
// 	const definition = {
// 		model: {
// 			key: modelAttributeKey,
// 			values: []
// 		},
// 		view: {},
// 		upcastAlso: {}
// 	};



// 	for (const option of options) {
// 		definition.model.values.push(option.model);
// 		definition.view[option.model] = option.view;
// 		if (option.upcastAlso) {
// 			definition.upcastAlso[option.model] = option.upcastAlso;
// 		}
// 	}

// 	return definition;
// }
