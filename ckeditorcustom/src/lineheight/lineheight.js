import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import LineheightEditing from './lineheightediting';

export default class Lineheight extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get requires() {
		return [ LineheightEditing ];
	}

	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Lineheight';
	}
}
