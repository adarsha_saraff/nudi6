import MetaData from "./metadata";
import FontTable from './core/fonttable';
import ColorTable from './core/colortable';
import NodeRtf, { CSSStyles } from './core/node';
import RtfParagraphNode from "./core/paragraph";
import RtfRootNode from "./core/rootnode";
import RtfListNode from "./core/listnode";
import RtfTextNode from "./core/text";

export default class RtfDocument {

    public metadata: MetaData;
    public fontTable: FontTable;
    public colorTable: ColorTable;

    public root: NodeRtf;

    public constructor() {
        this.metadata = new MetaData();
        this.fontTable = new FontTable();
        this.colorTable = new ColorTable();
        this.root = new RtfRootNode();
    }

    public addColor(red : number, blue: number, green : number){
       return this.colorTable.addColor(red,blue,green);
    }

    public addFont(fontFamily: string) {
        return this.fontTable.addFont(fontFamily);
    }

    public addParagraph(alignment: string) {
        var rtfPg: RtfParagraphNode = new RtfParagraphNode();
        if (alignment && alignment !== 'left') {
            rtfPg.cssStyle.textAlignment = alignment;
        } else {
            rtfPg.cssStyle = undefined;
        }
        rtfPg.parent = this.root;
        this.root.children.push(rtfPg);
        return rtfPg;
    }

    public addTextBlock(parent: RtfParagraphNode, text: string, style: CSSStyles) {
        var rtftxt: RtfTextNode = new RtfTextNode(text);
        rtftxt.cssStyle = style;
        parent.insertText(rtftxt);
        return rtftxt;
    }

    public addList(parent: RtfParagraphNode, ordered: boolean) {
        var rtfli: RtfListNode = new RtfListNode();
        if (ordered)
            rtfli.setAsOrdered();
        parent.insertList(rtfli);
        return rtfli;
    }

}