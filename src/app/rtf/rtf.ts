import RtfDocument from './document';
import RtfWriter from './writer/writer';

export default class Rtf {

    public static createDocument(){
        return new RtfDocument();
    }

    public static getDocumentWriter(doc : RtfDocument){
        return new RtfWriter(doc);
    }
}