import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';
import {FontService} from './providers/font.service';
import {MenuService} from './providers/menu.service';
import {RtfpluginService} from './providers/rtfplugin.service';

import { WebviewDirective } from './directives/webview.directive';
import {TabViewModule} from 'primeng/tabview';
import {SelectButtonModule} from 'primeng/selectbutton';
import {DropdownModule} from 'primeng/dropdown';
import {SliderModule} from 'primeng/slider';
import {ColorPickerModule} from 'primeng/colorpicker';
import {TooltipModule} from 'primeng/tooltip';
import {DialogModule} from 'primeng/dialog';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TitlebarComponent } from './components/titlebar/titlebar.component';
import { MenubarComponent } from './components/menubar/menubar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';



// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    TitlebarComponent,
    MenubarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    CKEditorModule,
    TabViewModule,
    SelectButtonModule,
    DropdownModule,
    SliderModule,
    DialogModule,
    TooltipModule,
    FontAwesomeModule,
    ColorPickerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  providers: [ElectronService, FontService, MenuService, RtfpluginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
