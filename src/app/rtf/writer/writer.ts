import RtfDocument from "../document";
import RtfNode from "../core/node";
import RtfParagraphNode from "../core/paragraph";
import RtfTextNode from "../core/text";
import Utils from '../Util';
import { FontTbl } from "../core/fonttable";

export default class RtfWriter {

    document: RtfDocument;
    charSets: Array<string> = [
        "\\ansi"
    ];

    constructor(document: RtfDocument) {
        this.document = document;
    }

    public createRtf() {
        const body = this.convertNodes();
        var stream = "{\\rtf1" + this.charSets.join('');
        stream += "\\deff0";
        stream += this.document.fontTable.toRTF();
        stream += "{\\*\\generator nudiRtfExport 1.0.0}\\viewkind4\\uc1";
        stream += this.document.metadata.toRTF();
        stream += body;
        return stream;
    }

    convertNodes() {
        const doc = this.document;
        var lines: Array<string> = [];
        for (let i = 0; i < doc.root.children.length; i++) {
            const node: RtfNode = doc.root.children[i];
            if (node.name === 'p') {
                lines.push(this.parseParagraphNode(node as RtfParagraphNode));
            }
        }
        return lines.join('\n\r');
    }

    parseParagraphNode(node: RtfParagraphNode) {
        if (node.children.length == 0) {
            return "\\par"; //empty paragraph == new line
        }
        let align = "\\ql";
        if (node.cssStyle)
            align = Utils.toAlignment(node.cssStyle.textAlignment);
        var lines: Array<string> = [];
        for (let i = 0; i < node.children.length; i++) {
            const n: RtfNode = node.children[i];
            if (n.name === 'p') {
                throw "invalid construction - p inside p";
            } else if (n.name === 'span') {
                const content: string = (n as RtfTextNode).text;
                if (n.cssStyle) {
                    var text: string = "";
                    node.clearStyle = true;
                    if (n.cssStyle.fontFamily) {
                        const ft: FontTbl = this.document.fontTable.findFont(n.cssStyle.fontFamily);
                        if (ft) {
                            text += `\\f${ft.index}`;
                        }
                    }
                    if (n.cssStyle.fontSize != 11) {
                        text += `\\fs${Utils.fromPoint(n.cssStyle.fontSize)}`;
                    }
                    text += ((n.cssStyle.bold ? "\\b" : "") + (n.cssStyle.italic ? "\\i" : "") +
                        (n.cssStyle.underline ? "\\ul" : "") +
                        (n.cssStyle.strikethrough ? "\\strike" : ""));
                    text += this.encodeText(content);
                    text += ((n.cssStyle.strikethrough ? "\\strike0" : "") + (n.cssStyle.underline ? "\\ul0" : "") +
                        (n.cssStyle.italic ? "\\i0" : "") +
                        (n.cssStyle.bold ? "\\b0" : ""));
                    lines.push(text);
                } else {
                    lines.push(this.encodeText(content));
                }
            }
        }
        return ((node.clearStyle ? "\\pard" : "") + (align === "\\ql" ? "" : align) + lines.join('') + "\\par");
    }

    encodeText(text: string) {
        var textLine: Array<string> = [];
        for (let i = 0; i < text.length; i++) {
            var ch = text.charAt(i);
            if (Utils.isAscii(ch)) {
                if (Utils.isControlChar(ch)) {
                    textLine.push(`\\${ch}`);
                } else {
                    textLine.push(ch);
                }
            } else {
                this.addCharset("\\ansicpg1252")
                const decimal = Utils.encodeUtf(ch);
                for (let j = 0; j < decimal.length; j++)
                    textLine.push(`\\u${decimal[j]}?`);
            }
        }
        return textLine.join('');
    }

    addCharset(set: string) {
        const f = (this.charSets).find(c => (c === set), set);
        if (!f) {
            this.charSets.push(set);
        }
    }

}